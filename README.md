# Arbetsprov - Insthink

## Beskrivning
Arbetsprovet består av att skapa en "osäker" lösenordshanterare med hjälp av MeteorJS och React. Tanken är att webappen som skapas, ska bestå av två enkla vyer. I den ena vyn listas
alla existerande lösenord och användarnamn som sparats i databasen. I den andra vyn så har användaren möjlighet att lägga till nya lösenord i databasen. Det är inga som helst krav på inloggning eller annan form av autentisering eller kryptering. Det är inte heller några krav på data-validering eller liknande. Det viktiga är att din lösning kan skicka in data genom så kallade "Meteor Methods" som sen skrivs till databasen och visas upp för användaren. Nedan följer två enkla skisser över vad vyerna ska innehålla, designen är helt upp till dig. Det räcker med att gränssnittet ser bra ut för en vanlig mobile viewport (ex. iPhone 7), det är inga krav på responsivitet.

---

### Vy 1 - Lista

![List vy](https://gitlab.com/InsthinkPublic/Arbetsprov/raw/master/img/listview.png)

### Vy 2 - Nytt lösenord

![Input vy](https://gitlab.com/InsthinkPublic/Arbetsprov/raw/master/img/inputview.png)

---

## Installation
Git-repot är redan initialierat med en aktuell version av Meteor och innehåller lite boilerplate kod. Däremot behöver du installera Meteor lokalt för att kunna starta servern, detta görs enklast med hjälpa av terminalen (OSX/Linux):
```
curl https://install.meteor.com/ | sh
``` 

För att installera alla dependencies så räcker det med att köra kommandot nedan  (inne i Git-repot.)

```
meteor npm install
```

Sen för att starta servern så kör du kommandot:

```
meteor
```

Nu kan du besöka `localhost:3000` i din webbläsare och du borde då se en enkel placeholder sida.

---

## Inlämning
Lämna in din lösning genom att skapa ett zip-arkiv av hela projektet och maila till någon av oss på Insthink.

